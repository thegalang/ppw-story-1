from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
name = 'Galangkangin Gotera'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 2, 29) 
npm = 1806133875 
kuliah = 'Universitas Indonesia'
img_dir = "https://files.catbox.moe/m1s23b.jpg"
hobby = 'Makan, ngeblog, ngememe'
angkatan = 'Quanta, 2018'
desc = """Saya seorang dari Bali yang suka komputer. Alasan saya kuliah di UI adalah agar bisa mengambis dengan puas. Saya sendiri pengalaman ngoding web masih minim jadi mohon bantuannya ya... """
# Create your views here.
def index(request):
    response = {'name': name, 'age': calculate_age(birth_date.year), 'npm': npm, 'angkatan' : angkatan,
    			'kuliah' : kuliah, 'hobi' : hobby, 'desc' : desc, 'img_dir' : img_dir}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
